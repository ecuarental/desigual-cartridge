var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var shell = require('gulp-shell');
var webdav = require('gulp-webdav-sync');
var config = require('./config.json');
var watch = require('gulp-watch');
var sourcemaps = require('gulp-sourcemaps');

var host = config.sandbox.host;
var pathname = config.sandbox.webdav;
var protocol = "https://";
var user = config.credentials.user;
var pass = config.credentials.pass;
var href = protocol + user + ":" + pass + "@" + host + "443" + pathname;
var site_name = config.sandbox.site;

var cartridge = 'dsg_branding/cartridge/';

// Browserify proxy
var proxy = 'https://' + host + "/on/demandware.store/Sites-" + site_name + "-Site";

// WebDAV options array
var options = {
  protocol: 'https:',
  auth: {
    'user': user,
    'pass': pass,
    'sendImmediately': true // use http digest authentication
  },
  hostname: host
    // , port: 443
    ,
  pathname: pathname,
  log: 'info' // show status codes
    ,
  logAuth: true,
  uselastmodified: true
}

/* gulp.task('sass', function () {
  gulp.src(cartridge + 'scss/default/style.scss')
    .pipe(sass({
      includePaths: ['scss']
    }))
    .pipe(gulp.dest(cartridge + 'static/default/css'))
}); */

gulp.task('sass', function() {
  return gulp.src(cartridge + 'scss/default/**/*.scss')
  .pipe(sourcemaps.init())
  .pipe(sass().on('error', sass.logError))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(cartridge + 'static/default/css'))
});

gulp.task('browser-sync', function () {
  browserSync.init([cartridge + "static/default/css/*.css"], {
    proxy: proxy,
    // proxy: "http://ecuarental-inside-sfdc-eu01-dw.demandware.net/on/demandware.store/Sites-NTO-Site",
    reloadDelay: 2000,
    reloadDebounce: 2000,
    injectChanges: false
  })
});

gulp.task('deploy-css', ['sass'], function () {
  return gulp.src([cartridge + 'static/default/css/style.css'])
    .pipe(webdav(options))
})

gulp.task('default', ['sass', 'deploy-css', 'browser-sync'], function () {
  gulp.watch([cartridge + "scss/default/**/*.scss",
      cartridge + "static/default/css/*.css"
    ], ['sass', 'deploy-css'])
    .on('change', webdav(href, options).watch)
    .on('change', browserSync.reload)
});

gulp.task('deploy', function () {
  return gulp.src(cartridge + "**")
    .pipe(webdav(options))
})

gulp.task('deploy-tpl', function () {
  return gulp.src(cartridge + "templates/**")
    .pipe(webdav(options))
})

gulp.task('deploy-js', function () {
  return gulp.src([cartridge + "static/default/js/**",
      cartridge + "scripts/**"
    ])
    .pipe(webdav(options))
})

gulp.task('watch-dev', ['sass', 'deploy-css', 'deploy-tpl', 'deploy-js', 'browser-sync'], function () {
  gulp.watch([cartridge + "scss/default/**/*.scss",
    cartridge + "static/default/css/*.css",
    cartridge + "templates/**",
    cartridge + "static/default/js/**",
    cartridge + "scripts/**"
  ], ['sass', 'deploy-css', 'deploy-tpl', 'deploy-js'])
  //.on('change', webdav(href, options).watch)
  //.on('change', browserSync.reload)
});

paths = [cartridge + "scss/default/**/*.scss",
  cartridge + "static/default/css/*.css",
  cartridge + "templates/**",
  cartridge + "static/default/js/**"
];

gulp.task('deploy-partial', function () {
  return gulp.src(paths)
    .pipe(webdav(options))
})

gulp.task('watch-new', ['sass', 'browser-sync'], function () {
  gulp.watch(
    [cartridge + "scss/default/**/*.scss"], ['sass']
  ).on('change', browserSync.reload);

  gulp.watch([cartridge + "templates/**"])
    .on('change', browserSync.reload);

  gulp.watch(
    [cartridge + "static/default/js/**",
      cartridge + "scripts/**"
    ]
  ).on('change', browserSync.reload);
  //.on('change', webdav(href, options).watch)
  //.on('change', browserSync.reload)
});

gulp.task('default', ['watch-new']);