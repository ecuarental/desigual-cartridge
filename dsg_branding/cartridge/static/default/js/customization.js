$(document).ready(function() {
  $('.my-carousel').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    prevArrow: '<button type="button" class="slick-prev">&lt;</button>',
    nextArrow: '<button type="button" class="slick-next">&gt;</button>',
    mobileFirst: false,
    autoplay: true
  });
})
