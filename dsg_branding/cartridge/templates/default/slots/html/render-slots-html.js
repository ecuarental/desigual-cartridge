var fs = require('fs')
var handlebars = require('handlebars')

var template_source = fs.readFileSync('home-main-template-treatments.html','utf8');

console.log(template_source)

var template = handlebars.compile(template_source);

var data = JSON.parse(fs.readFileSync('home-main-data.json','utf8'))

var result = template(data);

fs.writeFileSync('home-main-rituals-01.isml', result);

function render_to_file(template, data, out_file) {
 result = template(data);
 fs.writeFileSync(out_file, result);
};

data.forEach(function(item) {
  render_to_file(template, item['data'], item['outfile']);
});
